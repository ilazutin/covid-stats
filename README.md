# COVID-19 statistics

Flutter app for analyze statistics of COVID-19 cases

This project is created for tutorial purposes

COVID-19 data sourced from Worldometers, updated every 10 minutes

Application uses API from https://github.com/disease-sh/API

API's docs is available on https://disease.sh/docs/#/

## App sample
![covid-stats](./git_img/sample.gif)
