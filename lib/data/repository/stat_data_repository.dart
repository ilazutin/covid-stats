
import 'package:covid_stats/data/api/stats_api.dart';
import 'package:covid_stats/domain/model/country.dart';
import 'package:covid_stats/domain/model/summary_stats.dart';
import 'package:covid_stats/domain/repository/repository.dart';
import 'package:covid_stats/data/mappers/summary_mapper.dart';
import 'package:covid_stats/data/mappers/country_mapper.dart';
import 'package:dio/dio.dart';

class StatsDataRepository extends StatsRepository {

  @override
  Future<SummaryStats> getAll() async {
    return SummaryMapper.fromDto((await StatsApi(Dio()).getAll()));
  }

  @override
  Future<List<Country>> getByCountries() async {
    return (await StatsApi(Dio()).getByCountries())
        .map((country) => CountryMapper.fromDto(country)).toList();
  }

}