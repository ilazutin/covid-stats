import 'package:covid_stats/data/model/countries_dto.dart';
import 'package:covid_stats/domain/model/country.dart';
import 'package:covid_stats/domain/model/stats.dart';

class CountryMapper {

  static Country fromDto(CountryDto summary) {
    return Country(
        summary.country,
        summary.countryInfo.iso3,
        summary.countryInfo.flag,
        summary.continent,
        Stats(
            DateTime.fromMillisecondsSinceEpoch(summary.updated),
            summary.cases,
            todayCases: summary.todayCases,
            activePerOneMillion: summary.activePerOneMillion,
            casesPerOneMillion: summary.casesPerOneMillion,
            critical: summary.critical,
            criticalPerOneMillion: summary.criticalPerOneMillion,
            deaths: summary.deaths,
            todayDeaths: summary.todayDeaths,
            deathsPerOneMillion: summary.deathsPerOneMillion,
            oneCasePerPeople: summary.oneCasePerPeople,
            oneDeathPerPeople: summary.oneDeathPerPeople,
            oneTestPerPeople: summary.oneTestPerPeople,
            population: summary.population,
            recovered: summary.recovered,
            recoveredPerOneMillion: summary.recoveredPerOneMillion,
            tests: summary.tests,
            testsPerOneMillion: summary.testsPerOneMillion,
            todayRecovered: summary.todayRecovered
        ));
  }
}