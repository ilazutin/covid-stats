import 'package:covid_stats/data/model/summary_dto.dart';
import 'package:covid_stats/domain/model/summary_stats.dart';
import 'package:covid_stats/domain/model/stats.dart';

class SummaryMapper {

  static SummaryStats fromDto(SummaryStatsDto summary) {
    return SummaryStats(
        Stats(
          DateTime.fromMillisecondsSinceEpoch(summary.updated),
          summary.cases,
          todayCases: summary.todayCases,
          activePerOneMillion: summary.activePerOneMillion,
          casesPerOneMillion: summary.casesPerOneMillion,
          critical: summary.critical,
          criticalPerOneMillion: summary.criticalPerOneMillion,
          deaths: summary.deaths,
          deathsPerOneMillion: summary.deathsPerOneMillion,
          oneCasePerPeople: summary.oneCasePerPeople,
          oneDeathPerPeople: summary.oneDeathPerPeople,
          oneTestPerPeople: summary.oneTestPerPeople,
          population: summary.population,
          recovered: summary.recovered,
          recoveredPerOneMillion: summary.recoveredPerOneMillion,
          tests: summary.tests,
          testsPerOneMillion: summary.testsPerOneMillion,
          todayRecovered: summary.todayRecovered
        ),
        summary.affectedCountries);
  }
}