import 'package:json_annotation/json_annotation.dart';
import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';
import 'package:covid_stats/data/model/summary_dto.dart';
import 'package:covid_stats/data/model/countries_dto.dart';

part 'stats_api.g.dart';

@RestApi(baseUrl: 'https://disease.sh/v3/covid-19')
abstract class StatsApi {

  factory StatsApi(Dio dio, {String baseUrl}) = _StatsApi;

  @GET("/all")
  Future<SummaryStatsDto> getAll();

  @GET("/countries")
  Future<List<CountryDto>> getByCountries();
}