// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'stats_api.dart';

// **************************************************************************
// RetrofitGenerator
// **************************************************************************

class _StatsApi implements StatsApi {
  _StatsApi(this._dio, {this.baseUrl}) {
    ArgumentError.checkNotNull(_dio, '_dio');
    baseUrl ??= 'https://disease.sh/v3/covid-19';
  }

  final Dio _dio;

  String baseUrl;

  @override
  Future<SummaryStatsDto> getAll() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.request<Map<String, dynamic>>('/all',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    final value = SummaryStatsDto.fromJson(_result.data);
    return value;
  }

  @override
  Future<List<CountryDto>> getByCountries() async {
    const _extra = <String, dynamic>{};
    final queryParameters = <String, dynamic>{};
    final _data = <String, dynamic>{};
    final _result = await _dio.request<List<dynamic>>('/countries',
        queryParameters: queryParameters,
        options: RequestOptions(
            method: 'GET',
            headers: <String, dynamic>{},
            extra: _extra,
            baseUrl: baseUrl),
        data: _data);
    var value = _result.data
        .map((dynamic i) => CountryDto.fromJson(i as Map<String, dynamic>))
        .toList();
    return value;
  }
}
