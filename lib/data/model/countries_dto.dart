import 'package:json_annotation/json_annotation.dart';

part 'countries_dto.g.dart';

@JsonSerializable()
class CountryDto {

  final String country;
  final CountryInfoDto countryInfo;
  final String continent;
  final int updated;
  final double cases;
  final double todayCases;
  final double deaths;
  final double todayDeaths;
  final double recovered;
  final double todayRecovered;
  final double critical;
  final double casesPerOneMillion;
  final double deathsPerOneMillion;
  final double tests;
  final double testsPerOneMillion;
  final double population;
  final double oneCasePerPeople;
  final double oneDeathPerPeople;
  final double oneTestPerPeople;
  final double activePerOneMillion;
  final double recoveredPerOneMillion;
  final double criticalPerOneMillion;

  CountryDto(
      this.country,
      this.countryInfo,
      this.continent,
      this.updated,
      this.cases,
      this.todayCases,
      this.deaths,
      this.todayDeaths,
      this.recovered,
      this.todayRecovered,
      this.critical,
      this.casesPerOneMillion,
      this.deathsPerOneMillion,
      this.tests,
      this.testsPerOneMillion,
      this.population,
      this.oneCasePerPeople,
      this.oneDeathPerPeople,
      this.oneTestPerPeople,
      this.activePerOneMillion,
      this.recoveredPerOneMillion,
      this.criticalPerOneMillion);

  factory CountryDto.fromJson(Map<String, dynamic> json) => _$CountryDtoFromJson(json);
  Map<String, dynamic> toJson() => _$CountryDtoToJson(this);

}

@JsonSerializable()
class CountryInfoDto {
  final String iso2;
  final String iso3;
  final String flag;

  CountryInfoDto(this.iso2, this.iso3, this.flag);

  factory CountryInfoDto.fromJson(Map<String, dynamic> json) => _$CountryInfoDtoFromJson(json);
  Map<String, dynamic> toJson() => _$CountryInfoDtoToJson(this);
}