// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'summary_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SummaryStatsDto _$SummaryStatsDtoFromJson(Map<String, dynamic> json) {
  return SummaryStatsDto(
    json['updated'] as int,
    (json['cases'] as num)?.toDouble(),
    (json['todayCases'] as num)?.toDouble(),
    (json['deaths'] as num)?.toDouble(),
    (json['recovered'] as num)?.toDouble(),
    (json['todayRecovered'] as num)?.toDouble(),
    (json['critical'] as num)?.toDouble(),
    (json['casesPerOneMillion'] as num)?.toDouble(),
    (json['deathsPerOneMillion'] as num)?.toDouble(),
    (json['tests'] as num)?.toDouble(),
    (json['testsPerOneMillion'] as num)?.toDouble(),
    (json['population'] as num)?.toDouble(),
    (json['oneCasePerPeople'] as num)?.toDouble(),
    (json['oneDeathPerPeople'] as num)?.toDouble(),
    (json['oneTestPerPeople'] as num)?.toDouble(),
    (json['activePerOneMillion'] as num)?.toDouble(),
    (json['recoveredPerOneMillion'] as num)?.toDouble(),
    (json['criticalPerOneMillion'] as num)?.toDouble(),
    (json['affectedCountries'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$SummaryStatsDtoToJson(SummaryStatsDto instance) =>
    <String, dynamic>{
      'updated': instance.updated,
      'cases': instance.cases,
      'todayCases': instance.todayCases,
      'deaths': instance.deaths,
      'recovered': instance.recovered,
      'todayRecovered': instance.todayRecovered,
      'critical': instance.critical,
      'casesPerOneMillion': instance.casesPerOneMillion,
      'deathsPerOneMillion': instance.deathsPerOneMillion,
      'tests': instance.tests,
      'testsPerOneMillion': instance.testsPerOneMillion,
      'population': instance.population,
      'oneCasePerPeople': instance.oneCasePerPeople,
      'oneDeathPerPeople': instance.oneDeathPerPeople,
      'oneTestPerPeople': instance.oneTestPerPeople,
      'activePerOneMillion': instance.activePerOneMillion,
      'recoveredPerOneMillion': instance.recoveredPerOneMillion,
      'criticalPerOneMillion': instance.criticalPerOneMillion,
      'affectedCountries': instance.affectedCountries,
    };
