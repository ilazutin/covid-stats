// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'countries_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CountryDto _$CountryDtoFromJson(Map<String, dynamic> json) {
  return CountryDto(
    json['country'] as String,
    json['countryInfo'] == null
        ? null
        : CountryInfoDto.fromJson(json['countryInfo'] as Map<String, dynamic>),
    json['continent'] as String,
    json['updated'] as int,
    (json['cases'] as num)?.toDouble(),
    (json['todayCases'] as num)?.toDouble(),
    (json['deaths'] as num)?.toDouble(),
    (json['todayDeaths'] as num)?.toDouble(),
    (json['recovered'] as num)?.toDouble(),
    (json['todayRecovered'] as num)?.toDouble(),
    (json['critical'] as num)?.toDouble(),
    (json['casesPerOneMillion'] as num)?.toDouble(),
    (json['deathsPerOneMillion'] as num)?.toDouble(),
    (json['tests'] as num)?.toDouble(),
    (json['testsPerOneMillion'] as num)?.toDouble(),
    (json['population'] as num)?.toDouble(),
    (json['oneCasePerPeople'] as num)?.toDouble(),
    (json['oneDeathPerPeople'] as num)?.toDouble(),
    (json['oneTestPerPeople'] as num)?.toDouble(),
    (json['activePerOneMillion'] as num)?.toDouble(),
    (json['recoveredPerOneMillion'] as num)?.toDouble(),
    (json['criticalPerOneMillion'] as num)?.toDouble(),
  );
}

Map<String, dynamic> _$CountryDtoToJson(CountryDto instance) =>
    <String, dynamic>{
      'country': instance.country,
      'countryInfo': instance.countryInfo,
      'continent': instance.continent,
      'updated': instance.updated,
      'cases': instance.cases,
      'todayCases': instance.todayCases,
      'deaths': instance.deaths,
      'todayDeaths': instance.todayDeaths,
      'recovered': instance.recovered,
      'todayRecovered': instance.todayRecovered,
      'critical': instance.critical,
      'casesPerOneMillion': instance.casesPerOneMillion,
      'deathsPerOneMillion': instance.deathsPerOneMillion,
      'tests': instance.tests,
      'testsPerOneMillion': instance.testsPerOneMillion,
      'population': instance.population,
      'oneCasePerPeople': instance.oneCasePerPeople,
      'oneDeathPerPeople': instance.oneDeathPerPeople,
      'oneTestPerPeople': instance.oneTestPerPeople,
      'activePerOneMillion': instance.activePerOneMillion,
      'recoveredPerOneMillion': instance.recoveredPerOneMillion,
      'criticalPerOneMillion': instance.criticalPerOneMillion,
    };

CountryInfoDto _$CountryInfoDtoFromJson(Map<String, dynamic> json) {
  return CountryInfoDto(
    json['iso2'] as String,
    json['iso3'] as String,
    json['flag'] as String,
  );
}

Map<String, dynamic> _$CountryInfoDtoToJson(CountryInfoDto instance) =>
    <String, dynamic>{
      'iso2': instance.iso2,
      'iso3': instance.iso3,
      'flag': instance.flag,
    };
