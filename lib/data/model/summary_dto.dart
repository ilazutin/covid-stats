import 'package:json_annotation/json_annotation.dart';

part 'summary_dto.g.dart';

@JsonSerializable()
class SummaryStatsDto {

  final int updated;
  final double cases;
  final double todayCases;
  final double deaths;
  final double recovered;
  final double todayRecovered;
  final double critical;
  final double casesPerOneMillion;
  final double deathsPerOneMillion;
  final double tests;
  final double testsPerOneMillion;
  final double population;
  final double oneCasePerPeople;
  final double oneDeathPerPeople;
  final double oneTestPerPeople;
  final double activePerOneMillion;
  final double recoveredPerOneMillion;
  final double criticalPerOneMillion;
  final double affectedCountries;

  SummaryStatsDto(
      this.updated,
      this.cases,
      this.todayCases,
      this.deaths,
      this.recovered,
      this.todayRecovered,
      this.critical,
      this.casesPerOneMillion,
      this.deathsPerOneMillion,
      this.tests,
      this.testsPerOneMillion,
      this.population,
      this.oneCasePerPeople,
      this.oneDeathPerPeople,
      this.oneTestPerPeople,
      this.activePerOneMillion,
      this.recoveredPerOneMillion,
      this.criticalPerOneMillion,
      this.affectedCountries);

  factory SummaryStatsDto.fromJson(Map<String, dynamic> json) => _$SummaryStatsDtoFromJson(json);
  Map<String, dynamic> toJson() => _$SummaryStatsDtoToJson(this);

}