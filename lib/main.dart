import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:covid_stats/internal/application.dart';

void main() {
  debugDefaultTargetPlatformOverride = TargetPlatform.fuchsia;

  runApp(Application());
}


