
import 'package:covid_stats/domain/bloc/country/country_bloc.dart';
import 'package:covid_stats/domain/model/country.dart';
import 'package:covid_stats/presentation/screens/country_details.dart';
import 'package:covid_stats/presentation/styles/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class CountryListItem extends StatelessWidget {

  final Country stats;

  const CountryListItem(this.stats);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(
            MaterialPageRoute(
                builder: (context) {
                  return BlocProvider<CountryBloc>(
                    create: (context) => CountryBloc()..add(CountryDetails(stats)),
                    child: CountryDetailsScreen(),
                  );
                },
            )
        );
      },
      child: Card(
        elevation: 4.0,
        color: Styles.cardColor(),
        child: Container(
          margin: EdgeInsets.all(16.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              left(),
              right(),
            ],
          ),
        ),
      ),
    );
  }

  left() {
    var f = NumberFormat();
    var active = stats.stats.cases - stats.stats.deaths - stats.stats.recovered;
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            stats.country,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: Styles.countryTitle(),
          ),
          Padding(
            padding: EdgeInsets.only(top: 12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Cases: ${f.format(stats.stats.cases)} | Today: ${f.format(stats.stats.todayCases)} | Active: ${f.format(active)}',
                  style: Styles.countryValue(),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 4.0, bottom: 4.0),
                  child: Text(
                    'Deaths: ${f.format(stats.stats.deaths)} | Today: ${f.format(stats.stats.todayDeaths)}',
                    style: Styles.countryValue(),
                  ),
                ),
                Text(
                  'Recovered: ${f.format(stats.stats.recovered)} | Today: ${f.format(stats.stats.todayRecovered)} | Critical: ${f.format(stats.stats.critical)}',
                  style: Styles.countryValue(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  right() {
    return Icon(Icons.arrow_forward_ios);
  }

}