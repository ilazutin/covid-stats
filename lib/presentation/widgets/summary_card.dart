
import 'dart:ui';

import 'package:covid_stats/presentation/styles/styles.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class SummaryCard extends StatelessWidget {

  final String title;
  final double value;
  final Icon icon;

  SummaryCard(
      {this.title, this.value, Icon icon}
  ) :
        this.icon = icon ?? null;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 4.0,
      color: Styles.cardColor(),
      child: Container(
        margin: EdgeInsets.all(16.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            left(),
          ],
        ),
      ),
    );
  }

  left() {
    var f = NumberFormat();
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            f.format(value),
            style: Styles.cardValue(),
          ),
          Text(
            title,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: Styles.cardTitle(),
          ),
        ],
      ),
    );
  }

  // right(BuildContext context) {
  //   var diff = item.value - item.previous;
  //   var percent = (item.value / item.previous - 1) * 100;
  //   return Container(
  //     height: 40,
  //     width: 80,
  //     decoration: BoxDecoration(
  //       borderRadius: BorderRadius.all(Radius.circular(5)),
  //       color: diff >= 0 ? Theme.of(context).primaryColor : Theme.of(context).accentColor,
  //     ),
  //     margin: EdgeInsets.only(left: 10),
  //     child: Column(
  //       crossAxisAlignment: CrossAxisAlignment.center,
  //       mainAxisAlignment: MainAxisAlignment.center,
  //       children: <Widget>[
  //         Text(
  //           '${diff.toStringAsFixed(4)}',
  //           style: TextStyle(
  //             color: Color(0xffffffff),
  //             fontSize: 14,
  //             fontWeight: FontWeight.bold,
  //           ),
  //         ),
  //         Text(
  //           '${percent.toStringAsFixed(2)} %',
  //           style: TextStyle(
  //             color: Color(0xffeeeeee),
  //             fontSize: 10,
  //           ),
  //         ),
  //       ],
  //     ),
  //   );
  // }
}