
import 'package:covid_stats/domain/model/app_tab.dart';
import 'package:flutter/material.dart';

class TabSelector extends StatelessWidget {
  final AppTab activeTab;
  final Function(AppTab) onTabSelected;

  TabSelector({
    Key key,
    @required this.activeTab,
    @required this.onTabSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      currentIndex: AppTab.values.indexOf(activeTab),
      onTap: (index) => onTabSelected(AppTab.values[index]),
      items: AppTab.values.map((tab) {
        return BottomNavigationBarItem(
          icon: tabIcon(tab),
          label: tabLabel(tab),
        );
      }).toList(),
    );
  }

  String tabLabel(AppTab tab) {
    switch (tab) {
      case AppTab.global:
        return 'Global';
      case AppTab.countries:
        return 'Countries';
    }
    return 'Global';
  }

  Widget tabIcon(AppTab tab) {
    switch (tab) {
      case AppTab.global:
        return Icon(Icons.public_outlined);
      case AppTab.countries:
        return Icon(Icons.list);
    }
    return null;
  }

}