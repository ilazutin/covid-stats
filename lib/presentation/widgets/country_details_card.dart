
import 'dart:ui';

import 'package:covid_stats/presentation/styles/styles.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CountryDetailsCard extends StatelessWidget {

  final String title;
  final double value;
  final Icon icon;

  CountryDetailsCard(
      {this.title, this.value, Icon icon}
  ) :
        this.icon = icon ?? null;

  @override
  Widget build(BuildContext context) {
    var _format = NumberFormat();

    return Card(
      elevation: 4.0,
      color: Styles.cardColor(),
      child: Container(
        margin: EdgeInsets.all(16.0),
        child: ListTile(
          title: Text(_format.format(value), style: Styles.cardValue(),),
          subtitle: Text(title, style: Styles.cardTitle(),),
          trailing: icon,
        ),
      ),
    );
  }

}