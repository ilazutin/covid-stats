
import 'package:flutter/material.dart';

class Styles {

  static ThemeData theme() {
    return ThemeData.dark();
  }

  static TextStyle appTitle() {
    return TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 30);
  }

  static Color cardColor() {
    return Color(0xaa272e47).withOpacity(0.5);
  }

  static TextStyle cardValue() {
    return TextStyle(
      color: Color(0xffcccccc),
      fontSize: 40,
      fontWeight: FontWeight.bold,
    );
  }

  static TextStyle cardTitle() {
    return TextStyle(
      color: Color(0xff999999),
      fontSize: 20,
    );
  }

  static TextStyle countryValue() {
    return TextStyle(
      color: Color(0xffcccccc),
      fontSize: 12,
      fontWeight: FontWeight.bold,
    );
  }

  static TextStyle countryTitle() {
    return TextStyle(
      color: Color(0xff999999),
      fontSize: 30,
    );
  }

}
