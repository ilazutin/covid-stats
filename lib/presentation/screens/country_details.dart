
import 'package:covid_stats/domain/bloc/country/country_bloc.dart';
import 'package:covid_stats/domain/model/country.dart';
import 'package:covid_stats/presentation/widgets/country_details_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CountryDetailsScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CountryBloc, CountryState>(
        builder: (context, state) {
          Widget body;
          String title = '';
          if (state is CountryDetailsLoaded) {
            title = state.stats.country;
            body = _CountryDetailsScreen(state.stats);
          } else {
            body = Center(child: CircularProgressIndicator());
          }

          return Scaffold(
            appBar: AppBar(
              title: Text(title),
            ),
            backgroundColor: Theme.of(context).backgroundColor,
            body: body,
          );
        },
    );
  }

}

class _CountryDetailsScreen extends StatelessWidget {

  final Country stats;

  const _CountryDetailsScreen(this.stats);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
          padding: EdgeInsets.all(16),
          child: Column(
            children: [
              Expanded(
                  child: ListView(
                    children: [
                      CountryDetailsCard(
                        title: 'Population',
                        value: stats.stats.population,
                      ),
                      CountryDetailsCard(
                        title: 'Cases',
                        value: stats.stats.cases,
                      ),
                      CountryDetailsCard(
                        title: 'Today cases',
                        value: stats.stats.todayCases,
                      ),
                      CountryDetailsCard(
                        title: 'Recovered',
                        value: stats.stats.recovered,
                      ),
                      CountryDetailsCard(
                        title: 'Today recovered',
                        value: stats.stats.todayRecovered,
                      ),
                      CountryDetailsCard(
                        title: 'Active',
                        value: stats.stats.cases - stats.stats.recovered - stats.stats.deaths,
                      ),
                      CountryDetailsCard(
                        title: 'Critical',
                        value: stats.stats.critical,
                      ),
                      CountryDetailsCard(
                        title: 'Tests',
                        value: stats.stats.tests,
                      ),
                      CountryDetailsCard(
                        title: 'Deaths',
                        value: stats.stats.deaths,
                      ),
                      CountryDetailsCard(
                        title: 'Today deaths',
                        value: stats.stats.todayDeaths,
                      ),
                    ],
                  )
              )
            ],
          )
      ),
    );
  }

}