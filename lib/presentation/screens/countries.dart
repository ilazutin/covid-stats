
import 'package:covid_stats/domain/bloc/countires_sort/countries_sort_bloc.dart';
import 'package:covid_stats/domain/bloc/countries/countries_bloc.dart';
import 'package:covid_stats/domain/bloc/country/country_bloc.dart';
import 'package:covid_stats/domain/bloc/filtered_countries/filtered_countries_bloc.dart';
import 'package:covid_stats/domain/model/countries_sort.dart';
import 'package:covid_stats/domain/model/country.dart';
import 'package:covid_stats/presentation/widgets/coutry_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CountriesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FilteredCountriesBloc, FilteredCountriesState>(
      builder: (context, state) {
        if (state is FilteredCountriesLoaded) {
          return _CountriesScreenBody(state.stats);
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}

class _CountriesScreenBody extends StatelessWidget {

  final List<Country> stats;

  _CountriesScreenBody(this.stats);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
          padding: EdgeInsets.all(16),
          child: Column(
            children: [
              BlocProvider<CountriesSortBloc>(
                create: (context) => CountriesSortBloc(),
                child: Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Card(
                        child: TextFormField(
                          decoration: InputDecoration(
                              prefixIcon: Icon(
                                Icons.search,
                                color: Theme.of(context).accentColor,
                              ),
                              border: OutlineInputBorder(),
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Theme.of(context).accentColor)),
                              labelText: 'Search',
                              labelStyle: TextStyle(
                                  color: Theme.of(context).accentColor),
                              hintText: 'Enter country name'),
                          onChanged: (value) => BlocProvider
                              .of<FilteredCountriesBloc>(context)
                              .add(FilterUpdated(value, (BlocProvider.of<FilteredCountriesBloc>(context).state as FilteredCountriesLoaded).sort)),
                        ),
                      )
                    ),
                    SortMenu(),
                  ],
                ),
              ),
              Expanded(
                  child: RefreshIndicator(
                    onRefresh: () async => BlocProvider.of<CountriesBloc>(context).add(CountriesLoad()),
                    child: BlocProvider<CountryBloc>(
                      create: (context) => CountryBloc(),
                      child: ListView.builder(
                        physics: const AlwaysScrollableScrollPhysics(),
                        itemCount: stats.length,
                        itemBuilder: (context, index) {
                          return CountryListItem(stats[index]);
                        },
                      ),
                    ),
                  )
              ),
            ],
          )
      ),
    );
  }
}

class SortMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CountriesSortBloc, CountriesSort>(
      builder: (context, state) {
        return PopupMenuButton<CountriesSort>(
            padding: EdgeInsets.zero,
            icon: Icon(Icons.sort),
            onSelected: (value) {
              BlocProvider
                .of<FilteredCountriesBloc>(context)
                .add(FilterUpdated((BlocProvider.of<FilteredCountriesBloc>(context).state as FilteredCountriesLoaded).filter, value));
              BlocProvider.of<CountriesSortBloc>(context).add(CountriesSortUpdate(value));
            },
            itemBuilder: (context) {
              var menu = <PopupMenuItem<CountriesSort>>[
                CheckedPopupMenuItem(
                  value: CountriesSort.name,
                  checked: state == CountriesSort.name,
                  child: Text('Country'),
                ),
                CheckedPopupMenuItem(
                  value: CountriesSort.totalCases,
                  checked: state == CountriesSort.totalCases,
                  child: Text('Total cases'),
                ),
                CheckedPopupMenuItem(
                  value: CountriesSort.todayCases,
                  checked: state == CountriesSort.todayCases,
                  child: Text('Today cases'),
                ),
                CheckedPopupMenuItem(
                  value: CountriesSort.recovered,
                  checked: state == CountriesSort.recovered,
                  child: Text('Recovered'),
                ),
              ];
              return menu;
            }
        );
      },
    );
  }
}