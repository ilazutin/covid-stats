
import 'package:covid_stats/domain/bloc/summary/summary_bloc.dart';
import 'package:covid_stats/domain/model/summary_stats.dart';
import 'package:covid_stats/presentation/styles/styles.dart';
import 'package:covid_stats/presentation/widgets/summary_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class GlobalScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SummaryBloc, SummaryState>(
        builder: (context, state) {
          if (state is GlobalStatsLoaded) {
            return _GlobalScreenBody(state.stats);
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
    );
  }
}

class _GlobalScreenBody extends StatelessWidget {

  final SummaryStats stats;

  _GlobalScreenBody(this.stats);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: EdgeInsets.all(16),
        child: RefreshIndicator(
          onRefresh: () async => BlocProvider.of<SummaryBloc>(context).add(GlobalStatsLoad()),
          child: ListView(
            physics: const AlwaysScrollableScrollPhysics(),
            children: [
              Padding(
                padding: EdgeInsets.only(bottom: 20),
                child: Text('Global coronavirus information', style: Styles.appTitle()),
              ),
              SummaryCard(
                title: 'Total cases',
                value: stats.stats.cases,
              ),
              SummaryCard(
                title: 'Total recovered',
                value: stats.stats.recovered,
              ),
              SummaryCard(
                title: 'Total death',
                value: stats.stats.deaths,
              ),
            ],
          ),
        )
      ),
    );
  }
}