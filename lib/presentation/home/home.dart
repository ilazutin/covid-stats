
import 'package:covid_stats/domain/bloc/countries/countries_bloc.dart';
import 'package:covid_stats/domain/bloc/country/country_bloc.dart';
import 'package:covid_stats/domain/bloc/filtered_countries/filtered_countries_bloc.dart';
import 'package:covid_stats/domain/bloc/summary/summary_bloc.dart';
import 'package:covid_stats/domain/bloc/tab/tab_bloc.dart';
import 'package:covid_stats/domain/model/app_tab.dart';
import 'package:covid_stats/presentation/screens/countries.dart';
import 'package:covid_stats/presentation/screens/global.dart';
import 'package:covid_stats/presentation/widgets/teb_selector.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Home extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<SummaryBloc>(
              create: (context) => SummaryBloc()..add(GlobalStatsLoad())
          ),
          BlocProvider<CountriesBloc>(
              create: (context) => CountriesBloc(),
          ),
          BlocProvider<CountryBloc>(
            create: (context) => CountryBloc(),
          ),
        ],
        child: BlocProvider<TabBloc>(create: (context) => TabBloc(),
            child: BlocBuilder<TabBloc, AppTab>(
                builder: (context, activeTab) {

                  Widget body;
                  switch (activeTab) {
                    case AppTab.global:
                      body = GlobalScreen();
                      break;
                    case AppTab.countries:
                      body = BlocProvider<FilteredCountriesBloc>(
                          create: (context) => FilteredCountriesBloc(
                              parentBloc: BlocProvider.of<CountriesBloc>(context)),
                          child: CountriesScreen()
                      );
                      break;
                  }
                  return Scaffold(
                      backgroundColor: Theme.of(context).backgroundColor,
                      body: body,
                      bottomNavigationBar: TabSelector(
                        activeTab: activeTab,
                        onTabSelected: (tab) {
                          BlocProvider.of<TabBloc>(context).add(UpdateTab(tab));
                          switch (tab) {
                            case AppTab.global:
                              BlocProvider.of<SummaryBloc>(context).add(GlobalStatsLoad());
                              break;
                            case AppTab.countries:
                              BlocProvider.of<CountriesBloc>(context).add(CountriesLoad());
                              break;
                          }
                        },
                      )
                  );
                }
            )
        ),
    );
  }

}