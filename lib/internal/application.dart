import 'package:covid_stats/presentation/styles/styles.dart';
import 'package:flutter/material.dart';
import 'package:covid_stats/presentation/home/home.dart';

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'COVID-19 statistics',
      theme: Styles.theme(),
      home: Home(),
    );
  }
}