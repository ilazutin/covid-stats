import 'package:covid_stats/data/repository/stat_data_repository.dart';
import 'package:covid_stats/domain/repository/repository.dart';

class RepositoryModule {
  static StatsRepository _statsRepository;

  static StatsRepository statsRepository() {
    if (_statsRepository == null) {
      _statsRepository = StatsDataRepository();
    }
    return _statsRepository;
  }
}