part of 'summary_bloc.dart';

abstract class SummaryState extends Equatable {
  const SummaryState();
}

class SummaryInitial extends SummaryState {
  @override
  List<Object> get props => [];
}

class GlobalStatsLoaded extends SummaryState {

  final SummaryStats stats;

  const GlobalStatsLoaded(this.stats);

  @override
  List<Object> get props => [stats];

}