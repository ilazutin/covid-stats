part of 'summary_bloc.dart';

abstract class SummaryEvent extends Equatable {
  const SummaryEvent();
}

class GlobalStatsLoad extends SummaryEvent {

  @override
  List<Object> get props => [];

}