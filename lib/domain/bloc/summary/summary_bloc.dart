import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:covid_stats/domain/model/summary_stats.dart';
import 'package:covid_stats/internal/dependencies/repository_module.dart';
import 'package:equatable/equatable.dart';

part 'summary_event.dart';
part 'summary_state.dart';

class SummaryBloc extends Bloc<SummaryEvent, SummaryState> {
  SummaryBloc() : super(SummaryInitial());

  @override
  Stream<SummaryState> mapEventToState(
    SummaryEvent event,
  ) async* {
    if (event is GlobalStatsLoad) {

      SummaryStats stats = await RepositoryModule.statsRepository().getAll();
      yield GlobalStatsLoaded(stats);

    }
  }
}
