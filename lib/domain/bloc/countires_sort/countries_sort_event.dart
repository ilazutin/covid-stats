part of 'countries_sort_bloc.dart';

abstract class CountriesSortEvent extends Equatable {
  const CountriesSortEvent();
}

class CountriesSortUpdate extends CountriesSortEvent {
  final CountriesSort sort;

  CountriesSortUpdate(this.sort);

  @override
  List<Object> get props => [sort];

}