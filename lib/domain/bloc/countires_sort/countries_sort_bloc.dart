import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:covid_stats/domain/model/countries_sort.dart';
import 'package:equatable/equatable.dart';

part 'countries_sort_event.dart';

class CountriesSortBloc extends Bloc<CountriesSortEvent, CountriesSort> {
  CountriesSortBloc() : super(CountriesSort.name);

  @override
  Stream<CountriesSort> mapEventToState(
    CountriesSortEvent event,
  ) async* {
    if (event is CountriesSortUpdate) {
      yield event.sort;
    }
  }
}
