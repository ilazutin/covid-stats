part of 'filtered_countries_bloc.dart';

abstract class FilteredCountriesEvent extends Equatable {
  const FilteredCountriesEvent();
}

class FilterUpdated extends FilteredCountriesEvent {

  final String filter;
  final CountriesSort sort;

  const FilterUpdated(this.filter, this.sort);

  @override
  List<Object> get props => [filter, sort];

}

class FilteredCountriesUpdated extends FilteredCountriesEvent {

  final List<Country> stats;

  const FilteredCountriesUpdated(this.stats);

  @override
  List<Object> get props => [stats];

}