part of 'filtered_countries_bloc.dart';

abstract class FilteredCountriesState extends Equatable {
  const FilteredCountriesState();
}

class FilteredCountriesLoadInProgress extends FilteredCountriesState {
  @override
  List<Object> get props => [];
}

class FilteredCountriesLoaded extends FilteredCountriesState {

  final List<Country> stats;
  final String filter;
  final CountriesSort sort;

  const FilteredCountriesLoaded(this.stats, this.filter, this.sort);

  @override
  List<Object> get props => [stats, filter, sort];

}