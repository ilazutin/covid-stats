import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:covid_stats/domain/bloc/countries/countries_bloc.dart';
import 'package:covid_stats/domain/model/countries_sort.dart';
import 'package:covid_stats/domain/model/country.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

part 'filtered_countries_event.dart';
part 'filtered_countries_state.dart';

class FilteredCountriesBloc extends Bloc<FilteredCountriesEvent, FilteredCountriesState> {

  final CountriesBloc parentBloc;
  StreamSubscription subscription;

  FilteredCountriesBloc({@required this.parentBloc})
      : super(
    parentBloc.state is CountriesLoaded
        ? FilteredCountriesLoaded(
        (parentBloc.state as CountriesLoaded).stats, '', CountriesSort.name
    ) : FilteredCountriesLoadInProgress(),
  ) {
    subscription = parentBloc.listen((state) {
      if (state is CountriesLoaded) {
        add(FilteredCountriesUpdated((parentBloc.state as CountriesLoaded).stats));
      }
    });
  }

  @override
  Stream<FilteredCountriesState> mapEventToState(
    FilteredCountriesEvent event,
  ) async* {
    if (event is FilterUpdated) {
      yield* _mapUpdateFilterToState(event);
    } else if (event is FilteredCountriesUpdated) {
      yield* _mapCountriesUpdatedToState(event);
    }
  }

  Stream<FilteredCountriesState> _mapUpdateFilterToState(
      FilterUpdated event,
      ) async* {
    if (parentBloc.state is CountriesLoaded) {
      yield FilteredCountriesLoaded(
        _mapLists(
          (parentBloc.state as CountriesLoaded).stats,
          event.filter,
          event.sort,
        ),
        event.filter,
        event.sort,
      );
    }
  }

  Stream<FilteredCountriesState> _mapCountriesUpdatedToState(
      FilteredCountriesUpdated event,
      ) async* {
    final visibilityFilter = state is FilteredCountriesLoaded
        ? (state as FilteredCountriesLoaded).filter
        : '';
    final visibilitySort = state is FilteredCountriesLoaded
        ? (state as FilteredCountriesLoaded).sort
        : CountriesSort.name;
    yield FilteredCountriesLoaded(
      _mapLists(
        (parentBloc.state as CountriesLoaded).stats,
        visibilityFilter,
        visibilitySort,
      ),
      visibilityFilter,
      visibilitySort
    );
  }

  List<Country> _mapLists(
      List<Country> countries, String filter, CountriesSort sort) {
    var list = countries.where((country) {
      if (filter.isEmpty) {
        return true;
      } else if (country.country.toLowerCase().contains(filter.toLowerCase())) {
        return true;
      } else {
        return false;
      }
    }).toList();

    list.sort((a, b) {
      switch (sort) {
        case CountriesSort.name:
          return a.country.toLowerCase().compareTo(b.country.toLowerCase());
        case CountriesSort.totalCases:
          return b.stats.cases.compareTo(a.stats.cases);
        case CountriesSort.todayCases:
          return b.stats.todayCases.compareTo(a.stats.todayCases);
        case CountriesSort.recovered:
          return b.stats.recovered.compareTo(a.stats.recovered);
        default:
          return b.country.toLowerCase().compareTo(a.country.toLowerCase());
      }
    });

    return list;
  }

  @override
  Future<void> close() {
    subscription.cancel();
    return super.close();
  }
}
