import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:covid_stats/domain/model/country.dart';
import 'package:equatable/equatable.dart';

part 'country_event.dart';
part 'country_state.dart';

class CountryBloc extends Bloc<CountryEvent, CountryState> {
  CountryBloc() : super(CountryInitial());

  @override
  Stream<CountryState> mapEventToState(
    CountryEvent event,
  ) async* {
    if (event is CountryDetails) yield CountryDetailsLoaded(event.stats);
  }
}
