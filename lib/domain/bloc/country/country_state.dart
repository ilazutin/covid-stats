part of 'country_bloc.dart';

abstract class CountryState extends Equatable {
  const CountryState();
}

class CountryInitial extends CountryState {
  @override
  List<Object> get props => [];
}

class CountryDetailsLoaded extends CountryState {

  final Country stats;

  const CountryDetailsLoaded(this.stats);

  @override
  List<Object> get props => [stats];

}