part of 'country_bloc.dart';

abstract class CountryEvent extends Equatable {
  const CountryEvent();
}

class CountryDetails extends CountryEvent {

  final Country stats;

  CountryDetails(this.stats);

  @override
  List<Object> get props => [stats];

}