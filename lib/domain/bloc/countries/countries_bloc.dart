import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:covid_stats/domain/model/country.dart';
import 'package:covid_stats/internal/dependencies/repository_module.dart';
import 'package:equatable/equatable.dart';

part 'countries_event.dart';
part 'countries_state.dart';

class CountriesBloc extends Bloc<CountriesEvent, CountriesState> {
  CountriesBloc() : super(CountriesInitial());

  @override
  Stream<CountriesState> mapEventToState(
    CountriesEvent event,
  ) async* {
    if (event is CountriesLoad) {

      List<Country> stats = await RepositoryModule
          .statsRepository().getByCountries();

      yield CountriesLoaded(stats);

    }
  }
}
