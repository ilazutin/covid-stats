part of 'countries_bloc.dart';

abstract class CountriesState extends Equatable {
  const CountriesState();
}

class CountriesInitial extends CountriesState {
  @override
  List<Object> get props => [];
}

class CountriesLoaded extends CountriesState {

  final List<Country> stats;

  const CountriesLoaded(this.stats);

  @override
  List<Object> get props => [stats];
}