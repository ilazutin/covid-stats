part of 'countries_bloc.dart';

abstract class CountriesEvent extends Equatable {
  const CountriesEvent();
}

class CountriesLoad extends CountriesEvent {

  @override
  List<Object> get props => [];

}