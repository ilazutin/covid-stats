
import 'package:covid_stats/domain/model/stats.dart';

class SummaryStats {

  final double affectedCountries;
  final Stats stats;

  SummaryStats(this.stats, this.affectedCountries);

}