
import 'package:covid_stats/domain/model/stats.dart';

class Country {

  final String country;
  final String countryIso3;
  final String countryFlag;
  final String continent;
  final Stats stats;

  Country(
      this.country,
      this.countryIso3,
      this.countryFlag,
      this.continent,
      this.stats);

}