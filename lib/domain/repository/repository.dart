
import 'package:covid_stats/domain/model/country.dart';
import 'package:covid_stats/domain/model/summary_stats.dart';

abstract class StatsRepository {

  Future<SummaryStats> getAll();

  Future<List<Country>> getByCountries();

}